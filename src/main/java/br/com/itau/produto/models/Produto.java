package br.com.itau.produto.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

//** A anotação @Entity é utilizada para informar que uma classe também é uma entidade.                                     **
//** A partir disso, a JPA estabelecerá a ligação entre a entidade e uma tabela de mesmo nome no banco de dados             **
//** A anotação @Table é utilizada para refenrenciar o nome da tabela que estaremos utilizando

@Entity
@Table(name="produto")
public class Produto {

	//** A anotação @Id serve para identificar o campo chave da sua entidade.
	@Id
	private int idProduto;
	
	@NotBlank
	private String nomeProduto;

	public int getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
	
	
}
