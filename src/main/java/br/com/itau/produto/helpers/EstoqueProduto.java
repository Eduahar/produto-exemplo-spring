package br.com.itau.produto.helpers;

public class EstoqueProduto {

	private int idEstoqueProduto;

	private int quantidadeProdutos;

	public int getIdProduto() {
		return idEstoqueProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idEstoqueProduto = idProduto;
	}

	public int getQuantidadeProdutos() {
		return quantidadeProdutos;
	}

	public void setQuantidadeProdutos(int quantidadeProdutos) {
		this.quantidadeProdutos = quantidadeProdutos;
	}

}
