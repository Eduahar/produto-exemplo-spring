package br.com.itau.produto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.produto.helpers.ProdutoQuantidade;
import br.com.itau.produto.models.Produto;
import br.com.itau.produto.services.ProdutoService;

//** A anotação @RestController nos permite criar um controlador com características REST e que possa manipular as requisições 
@RestController

//** A anotação @RequestMapping serve para colocar o endereço da sua aplicação, um exemplo de como ficaria esta chamada
//**  ex http://localhost:8888/produto
@RequestMapping("/produto")
public class ProdutoController {

//** a grosso modo a anotação @Autowired funciona como um instanciação unica "por baixo dos panos"
	@Autowired
	ProdutoService produtoService;
	
	//** a anotação @GetMapping irá executar a requisição sem receber nenhum parametro		
	@GetMapping
	public Iterable<Produto> listarProduto() {
		return produtoService.listarProdutos();
	}
	
	//** a anotação @GetMapping("/{id}) irá executar a requisição recebendo um paramtro na url, ex de chamada: http://localhost:8888/produto/1 	
	@GetMapping("/{id}")
	
	//** a anotação @PathVariable é utilizado para o sistema entender que deveremos receber um parametro da url
	public ProdutoQuantidade consultarProdutoId(@PathVariable int id) {
		return produtoService.consultarProdutoId(id);
	}
	
	//** a anotação @PostMapping irá executar a requisição recebendo informações JSON
	//** como realizar a chamada deste end-point : http://localhost:8888/produto com a função de POST
	@PostMapping
	public void inserirProduto(@RequestBody ProdutoQuantidade produtoQuantidade) {
		
		//** a anotação @RequestBody basicamente é utilizado para o sistema receber as informações de entrada JSON na chamada da nossa aplicação
		produtoService.inserirProduto(produtoQuantidade);
	}
	
}
