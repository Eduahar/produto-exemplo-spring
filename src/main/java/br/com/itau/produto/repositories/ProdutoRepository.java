package br.com.itau.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.produto.models.Produto;

//**  O CRUDRepository é uma interface do Spring Data para operações de CRUD, 
//** ele disponibiliza alguns metódos padrões de consulta, inclusao, atualização, iremos ver a chamada desses metodos na
//** na classe EstoqueProdutoService

//** Especificação de como preencher os parametros do CRUDRepository<Entidade Criada "EstoqueProduto", O tipo da chave "@ID" da Entidade>

public interface ProdutoRepository extends CrudRepository<Produto, Integer>{

}
