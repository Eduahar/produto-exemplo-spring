package br.com.itau.produto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.itau.produto.helpers.EstoqueProduto;
import br.com.itau.produto.helpers.ProdutoQuantidade;
import br.com.itau.produto.models.Produto;
import br.com.itau.produto.repositories.ProdutoRepository;

//** Anotação @Service serve para definir uma classe como pertencente à camada de Serviço da aplicação
//** Basicamente no service devemos inserir a lógica do processamento do nosso programa

@Service
public class ProdutoService {

	//** a grosso modo a anotação @Autowired funciona como um instanciação unica "por baixo dos panos"
	@Autowired
	ProdutoRepository produtoRepository;
	
	// o RestTemplate fornece implementações que facilitam a montagem das requisições HTTP aos outros serviços
	RestTemplate restTemplate = new RestTemplate();
	
	public Iterable<Produto> listarProdutos(){
		
		//**  o metodo findAll irá listar todos os registros da tabela;
		return produtoRepository.findAll();
	}
	
	public ProdutoQuantidade consultarProdutoId(int id) {
		
		//**  realizando a chamada de um metodo do ProdutoRepository sem precisar realizar uma instanciação
		//**  devido a anotação @AutoWired
		//**  este metodo findById irá na tabela recuperar o registro pelo id unico, ex. Select * from produto where id = id 	
		Produto produto = produtoRepository.findById(id).get();
		
		// restTemplate.GetForObject - irá realizar uma chamada GET na API estoqueProduto, informando o ID para recuperarmos a informação da quantidade de produtos
		EstoqueProduto estoqueProduto = restTemplate.getForObject("http://localhost:8082/estoqueproduto/" +id, EstoqueProduto.class);
		
		ProdutoQuantidade produtoQuantidade = new ProdutoQuantidade();
		
		produtoQuantidade.setIdProduto(id);
		produtoQuantidade.setNomeProduto(produto.getNomeProduto());
		produtoQuantidade.setQuantidadeProduto(estoqueProduto.getQuantidadeProdutos());
		
		return produtoQuantidade;
	}
	
	public void inserirProduto(ProdutoQuantidade produtoQuantidade) {
		Produto produto = new Produto();
		produto.setIdProduto(produtoQuantidade.getIdProduto());
		produto.setNomeProduto(produtoQuantidade.getNomeProduto());
		
		//**   o metodo save irá realizar o insert do registro na tabela;
		produtoRepository.save(produto);
		
		EstoqueProduto estoqueProduto = new EstoqueProduto();
		
		estoqueProduto.setIdProduto(produtoQuantidade.getIdProduto());
		estoqueProduto.setQuantidadeProdutos(produtoQuantidade.getQuantidadeProduto());
		
		// restTemplate.postForObject - irá realizar uma chamada POST na API estoqueProduto, informando enviando o objeto estoqueProduto para inserirmos a quantidade de produtos
		restTemplate.postForObject("http://localhost:8082/estoqueproduto", estoqueProduto, EstoqueProduto.class);
	}
}
